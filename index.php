<?php
require_once "Classes/CreditInputValidator.php";
require_once "Classes/CustomerCreditResponse.php";
require_once "Classes/CreditDataManager.php";
require_once "Classes/CustomerCreditRequest.php";
require_once "Classes/RemoteCreditService.php";

function optionLoop($first_value, $second_value)
{
    for ($i = $first_value; $i <= $second_value; $i++) {
        echo '<option value=' . $i . '>' . $i . '</option>';
    }
}
?>


<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" xml:lang="en">
<head>
    <title>Credy demo</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="style/style.css" media="all"></link>
    <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="js/livevalidation_standalone.compressed.js"></script>
    <script type="text/javascript" src="js/validation.js"></script>

</head>
<body>
<form enctype='multipart/form-data'
      action='index.php'
      method='post'>
    <input type="hidden" name="hidden">

    <div class="cont">Please insert Your personal data
        <br>
        <table width="500" align="center">
            <tr>
                <th class="name">Name</th>
                <th class="input"><input type=text name=fname id="fname" style=width:150px;></th>
            </tr>
            <tr>
                <th class="name">Surname</th>
                <th class="input"><input type=text name=lname id="lname" style=width:150px;></th>
            </tr>
            <tr>
                <th class="name">E-mail</th>
                <th class="input"><input type=text name=email id="email" style=width:150px;></th>
            </tr>
            <tr>
                <th class="name">Confirm e-mail</th>
                <th class="input"><input type=text name=confirm_email id="confirm_email" style=width:150px;></th>
            </tr>
            <tr>
                <th class="name">Phone number</th>
                <th class="input"><input type=text name=phone id="phone" style=width:150px;></th>
            </tr>
            <tr>
                <th class="name">Postal index</th>
                <th class="input"><input type=text name=postal_index id="postal_index" style=width:150px;></th>
            </tr>
            <tr>
                <th class="name">City</th>
                <th class="input"><select name="city" id="city" style=width:150px;><br>
                        <option value=" Tallinn "> Tallinn</option>
                        <option value=" Tartu "> Tartu</option>
                        <option value=" Narva "> Narva</option>
                        <option value=" Pärnu "> Pärnu</option>
                        <option value=" Kohtla-Järve "> Kohtla-Järve</option>
                        <option value=" Maardu "> Maardu</option>
                        <option value=" Viljandi "> Viljandi</option>
                        <option value=" Rakvere "> Rakvere</option>
                        <option value=" Sillamäe "> Sillamäe</option>
                        <option value=" Kuressaare "> Kuressaare</option>
                        <option value=" Võru "> Võru</option>
                        <option value=" Valga "> Valga</option>
                        <option value=" Jõhvi "> Jõhvi</option>
                        <option value=" Haapsalu "> Haapsalu</option>
                        <option value=" Keila "> Keila</option>
                        <option value=" Paide "> Paide</option>
                        <option value=" Türi "> Türi</option>
                        <option value=" Tapa "> Tapa</option>
                        <option value=" Põlva "> Põlva</option>
                        <option value=" Kiviõli "> Kiviõli</option>
                        <option value=" Elva "> Elva</option>
                        <option value=" Saue "> Saue</option>
                        <option value=" Jõgeva "> Jõgeva</option>
                        <option value=" Rapla "> Rapla</option>
                        <option value=" Põltsamaa "> Põltsamaa</option>
                        <option value=" Paldiski "> Paldiski</option>
                        <option value=" Sindi "> Sindi</option>
                        <option value=" Kunda "> Kunda</option>
                        <option value=" Kärdla "> Kärdla</option>
                        <option value=" Kehra "> Kehra</option>
                        <option value=" Loksa "> Loksa</option>
                        <option value=" Räpina "> Räpina</option>
                        <option value=" Tõrva "> Tõrva</option>
                        <option value=" Narva-Jõesuu "> Narva-Jõesuu</option>
                    </select></th>
            </tr>
            <tr>
                <th class="name">Date of Birth</th>
                <th class="input"><select name="day">
                        <?php
                        optionLoop(1, 31);
                        ?>
                    </select>
                    <select name="month" id="month" style=width:100px;>
                        <option value="1"> January</option>
                        <option value="2"> February</option>
                        <option value="3"> March</option>
                        <option value="4"> April</option>
                        <option value="5"> May</option>
                        <option value="6"> June</option>
                        <option value="7"> July</option>
                        <option value="8"> August</option>
                        <option value="9"> September</option>
                        <option value="10"> October</option>
                        <option value="11"> November</option>
                        <option value="12"> December</option>
                    </select>

                    <select name="year">
                        <?php
                        for ($i = 2010; $i > 1900; $i--) {
                            echo '<option value=' . $i . '>' . $i . '</option>';
                        }

                        ?>
                    </select></th>
            </tr>
            <tr>
                <th class="name">Password</th>
                <th class="input"><input type=password name=pass id="pass" style=width:150px;></th>
            </tr>
            <tr>
                <th class="name">Confirm password</th>
                <th class="input"><input type=password name=confirm_pass id="confirm_pass" style=width:150px;></th>
            </tr>
            <tr>
                <th class="name">Address</th>
                <th class="input"><input type=text name=address id="address" style=width:150px;></th>
            </tr>
            <tr>
                <th class="name">ID card number</th>
                <th class="input"><input type=text name=id_c id="id_c" style=width:150px;></th>
            </tr>
            <tr>
                <th class="name">Net income: (EUR)</th>
                <th class="input"><input type=text name=neto_income id="neto_income" style=width:150px;></th>
            </tr>
        </table>
        <br>
        Loan information<br>
        <table width="500" align="center">
            <tr>
                <th class="name">Loan sum: (EUR)</th>
                <th class="input"><input type=text name=loan_s id="loan_s" style=width:150px;></th>
            </tr>
            <tr>
                <th class="name">Loan period:(days)</th>
                <th class="input"><select name="loan_p">
                        <?php
                        optionLoop(1, 31);
                        ?>
                    </select></th>
            </tr>
        </table>
        <br>
        Conditions<br>
        <table width="500" align="center">
            <tr>
                <th class="name">Accept general conditions</th>
                <th class="input"><input type="checkbox" id="accept_general_conditions"
                                         name="accept_general_conditions"></th>
            </tr>
            <tr>
                <th class="name">Accept loan contract</th>
                <th class="input"><input type="checkbox" id="accept_loan_contract" name="accept_loan_contract"></th>
            </tr>
            <tr>
                <th class="name">Accept terms personal data</th>
                <th class="input"><input type="checkbox" id="accept_terms_personal_data"
                                         name="accept_terms_personal_data"></th>
            </tr>
        </table>
        <br>
        <input type=submit name=action value=Post>
        <br>
</form>
<?php
include "inc/input_data.php";
?>
</div>

</body>
</html>