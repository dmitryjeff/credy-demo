$(function () {

    var fName = new LiveValidation("fname", {validMessage: " ", wait: 500 });
    fName.add(Validate.Presence, {failureMessage: "Please insert firstname", validMessage: " "});
    fName.add(Validate.Length, { maximum: 20, validMessage: " "});
    fName.add(Validate.Format, { pattern: /^[a-zA-ZÕÜÖÄõüöä][a-zA-ZÕÜÖÄõüöä]*$/, failureMessage: "Must contain only letters",
        validMessage: " "});
    fName.add(Validate.Exclusion, {within: [' '], partialMatch: true, failureMessage: "Spaces are not allowed.",
        validMessage: " "});

    var lName = new LiveValidation("lname", {validMessage: " ", wait: 500 });
    lName.add(Validate.Presence, {failureMessage: "Please insert lastname", validMessage: " "});
    lName.add(Validate.Length, { maximum: 20, validMessage: " "});
    lName.add(Validate.Format, { pattern: /^[a-zA-ZÕÜÖÄõüöä][a-zA-ZÕÜÖÄõüöä]*$/, failureMessage: "Must contain only letters",
        validMessage: " "});
    lName.add(Validate.Exclusion, {within: [' '], partialMatch: true, failureMessage: "Spaces are not allowed.",
        validMessage: " "});

    var f21 = new LiveValidation('email', {validMessage: " ", wait: 500 });
    f21.add(Validate.Email,
        { failureMessage: "Please check your e-mail",
            validMessage: " "
        });
    f21.add(Validate.Presence,
        { failureMessage: "Please insert your e-mail",
            validMessage: " "});

    var confirm_mail = new LiveValidation('confirm_email', {validMessage: " ", wait: 500 });
    confirm_mail.add(Validate.Confirmation, { match: 'email', failureMessage: "Not match" });

    var phone = new LiveValidation('phone', {validMessage: " ", wait: 500 });
    phone.add(Validate.Numericality, {failureMessage: "Only integers", validMessage: " " });
    phone.add(Validate.Presence, {failureMessage: "Please insert phone number", validMessage: " "});
    phone.add(Validate.Length, { minimum: 4, maximum: 25, validMessage: " ", failureMessage: "Must be from 4 to 25 numbers" });

    var postal_index = new LiveValidation('postal_index', {validMessage: " ", wait: 500 });
    postal_index.add(Validate.Numericality, {failureMessage: "Only numbers", validMessage: " " });
    postal_index.add(Validate.Presence, {failureMessage: "Please insert postal index", validMessage: " "});
    postal_index.add(Validate.Length, { is: 5, validMessage: " ", failureMessage: "Must be 5 numbers" });

    var fPass = new LiveValidation("pass", {validMessage: " ", wait: 500 });
    fPass.add(Validate.Presence, {failureMessage: "Please insert password", validMessage: " "});
    fPass.add(Validate.Length, { minimum: 6, maximum: 20, validMessage: " "});
    fPass.add(Validate.Format, { pattern: /^[A-Za-z0-9]*$/, failureMessage: "Check password",
        validMessage: " "});

    var fconfPass = new LiveValidation('confirm_pass', {validMessage: " ", wait: 500});
    fconfPass.add(Validate.Confirmation, { match: 'pass', validMessage: " "});

    var adrs = new LiveValidation("address", {validMessage: " ", wait: 500 });
    adrs.add(Validate.Presence, {failureMessage: "Please insert your adddress", validMessage: " "});
    adrs.add(Validate.Length, { maximum: 20, validMessage: " "});
    adrs.add(Validate.Format, { pattern: /^[a-zA-Z0-9ÕÜÖÄõüöä -][a-z0-9A-ZÕÜÖÄõüöä -]*$/, failureMessage: "Incorrect adress",
        validMessage: " "});

    var idc = new LiveValidation('id_c', {validMessage: " ", wait: 500 });
    idc.add(Validate.Numericality, {failureMessage: "Only numbers", validMessage: " " });
    idc.add(Validate.Presence, {failureMessage: "Please insert id card number", validMessage: " "});
    idc.add(Validate.Length, { is: 9, validMessage: " ", failureMessage: "Must be 9 numbers" });

    var ni = new LiveValidation('neto_income', {validMessage: " ", wait: 500 });
    ni.add(Validate.Numericality, {failureMessage: "Only integers", validMessage: " " });
    ni.add(Validate.Presence, {failureMessage: "Please insert neto income", validMessage: " "});
    ni.add(Validate.Length, {maximum: 25, validMessage: " "});

    var ls = new LiveValidation('loan_s', {validMessage: " ", wait: 500 });
    ls.add(Validate.Numericality, {failureMessage: "Only integers", validMessage: " " });
    ls.add(Validate.Presence, {failureMessage: "Please insert loan sum", validMessage: " "});
    ls.add(Validate.Length, {maximum: 25, validMessage: " "});

    var a_g_c = new LiveValidation('accept_general_conditions', {validMessage: "OK", wait: 500 });
    a_g_c.add(Validate.Acceptance, {failureMessage: "Please select"});

    var a_l_c = new LiveValidation('accept_loan_contract', {validMessage: "OK", wait: 500 });
    a_l_c.add(Validate.Acceptance, {failureMessage: "Please select"});

    var a_t_p_d = new LiveValidation('accept_terms_personal_data', {validMessage: "OK", wait: 500 });
    a_t_p_d.add(Validate.Acceptance, {failureMessage: "Please select"});


});