<?php
/**
 * Created by PhpStorm.
 * User: Dmitri
 * Date: 2.06.14
 * Time: 0:07
 */

namespace Classes;


class CreditDataManager
{

    /**
     * Creates object of customerCredit data, validates, sends it to external API sever and gets the message.
     */
    public function processInput($data)
    {
        $configuration = file_get_contents('./configuration.conf', true);
        $conf_data = json_decode($configuration);

        $fname = (strip_tags($data['fname']));
        $lname = trim(strip_tags($data['lname']));
        $email = trim(strip_tags($data['email']));
        $email_confirmation = trim(strip_tags($data['confirm_email']));
        $phone = trim(strip_tags($data['phone']));
        $postal_index = trim(strip_tags($data['postal_index']));
        $city = trim(strip_tags($data['city']));
        $month = trim(strip_tags($data['month']));
        $day = trim(strip_tags($data['day']));
        $year = trim(strip_tags($data['year']));
        $pass = trim(strip_tags($data['pass']));
        $pass_conf = trim(strip_tags($data['confirm_pass']));
        $address = strip_tags($data['address']);
        $id_c = trim(strip_tags($data['id_c']));
        $loan_s = trim(strip_tags($data['loan_s']));
        $loan_p = trim(strip_tags($data['loan_p']));
        $neto_income = trim(strip_tags($data['neto_income']));
        $url = $conf_data->credy_remote_url;
        $affiliate_identifier = $conf_data->credy_affiliate_identifier;
        $accept_general_conditions = trim(strip_tags($data['accept_general_conditions']));
        $accept_loan_contract = trim(strip_tags($data['accept_loan_contract']));
        $accept_terms_personal_data = trim(strip_tags($data['accept_terms_personal_data']));

        $customerCredit = new \Classes\CustomerCreditRequest($month, $day, $year, $accept_general_conditions, $accept_loan_contract, $accept_terms_personal_data);
        $customerCredit->setAddress($address);
        $customerCredit->setAffiliateIdentifier($affiliate_identifier);
        $customerCredit->setCity($city);
        $customerCredit->setEmail($email);
        $customerCredit->setEmailConfirmation($email_confirmation);
        $customerCredit->setFirstName($fname);
        $customerCredit->setIdCardNumber($id_c);
        $customerCredit->setLastName($lname);
        $customerCredit->setLoanPeriod($loan_p);
        $customerCredit->setLoanSum($loan_s);
        $customerCredit->setNetoIncome($neto_income);
        $customerCredit->setPassword($pass);
        $customerCredit->setPasswordConfirmation($pass_conf);
        $customerCredit->setPhone($phone);
        $customerCredit->setPostalIndex($postal_index);
        $customerCredit->setUrl($url);

        $validator = new \Classes\DataManager();
        $validator->dataValidator($customerCredit);
        $remote = new \Classes\RemoteCreditService();
        $flag = false;
        foreach ($validator->getValidationErrors() as $err) {
            if ($err) {
                $flag = true;
            }
        }
        if ($flag) {
            echo "validation error";
        } else {

        $response = $remote->sendData($customerCredit);
        echo $response->getResponseMessage();
        }
    }
} 