<?php
/**
 * Created by PhpStorm.
 * User: Dmitri
 * Date: 29.05.14
 * Time: 20:31
 */

namespace Classes;
class CustomerCreditRequest
{
    private $phone;


    private $city;
    private $birth_date;
    private $password;
    private $password_confirmation;
    private $address;
    private $id_card_number;
    private $loan_sum;
    private $loan_period;
    private $first_name;
    private $last_name;
    private $email;
    private $email_confirmation;
    private $postal_index;
    private $neto_income;
    private $affiliate_identifier;
    private $url;
    private $accept_general_conditions;
    private $accept_loan_contract;
    private $accept_terms_personal_data;

    public function __construct($month, $day, $year, $accept_general_conditions, $accept_loan_contract, $accept_terms_personal_data)
    {
        $this->birth_date = date("Y-m-d", mktime(0, 0, 0, $month, $day, $year));


        $this->accept_general_conditions = $accept_general_conditions;
        if ($this->accept_general_conditions == "on") {
            $this->accept_general_conditions = 1;
        } else {
            $this->accept_general_conditions = 0;
        }
        $this->accept_loan_contract = $accept_loan_contract;
        if ($this->accept_loan_contract == "on") {
            $this->accept_loan_contract = 1;
        } else {
            $this->accept_loan_contract = 0;
        }

        $this->accept_terms_personal_data = $accept_terms_personal_data;
        if ($this->accept_terms_personal_data == "on") {
            $this->accept_terms_personal_data = 1;
        } else {
            $this->accept_terms_personal_data = 0;
        }

    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->first_name;
    }

    /**
     * @return mixed
     */
    public function getIdCardnumber()
    {
        return $this->id_card_number;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->last_name;
    }

    /**
     * @return mixed
     */
    public function getLoanPeriod()
    {
        return $this->loan_period;
    }

    /**
     * @return mixed
     */
    public function getLoanSum()
    {
        return $this->loan_sum;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getEmailConfirmation()
    {
        return $this->email_confirmation;
    }

    /**
     * @return mixed
     */
    public function getPasswordConfirmation()
    {
        return $this->password_confirmation;
    }

    /**
     * @return mixed
     */
    public function getPostalIndex()
    {
        return $this->postal_index;
    }

    /**
     * @return mixed
     */
    public function getNetoIncome()
    {
        return $this->neto_income;
    }

    /**
     * @return mixed
     */
    public function getAffiliateIdentifier()
    {
        return $this->affiliate_identifier;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * @return mixed
     */
    public function getAcceptGeneralConditions()
    {
        return $this->accept_general_conditions;
    }

    /**
     * @return mixed
     */
    public function getAcceptLoanContract()
    {
        return $this->accept_loan_contract;
    }

    /**
     * @return mixed
     */
    public function getAcceptTermsPersonalData()
    {
        return $this->accept_terms_personal_data;
    }

    /**
     * @param int $accept_general_conditions
     */
    public function setAcceptGeneralConditions($accept_general_conditions)
    {
        $this->accept_general_conditions = $accept_general_conditions;
    }

    /**
     * @param int $accept_loan_contract
     */
    public function setAcceptLoanContract($accept_loan_contract)
    {
        $this->accept_loan_contract = $accept_loan_contract;
    }

    /**
     * @param int $accept_terms_personal_data
     */
    public function setAcceptTermsPersonalData($accept_terms_personal_data)
    {
        $this->accept_terms_personal_data = $accept_terms_personal_data;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @param mixed $affiliate_identifier
     */
    public function setAffiliateIdentifier($affiliate_identifier)
    {
        $this->affiliate_identifier = $affiliate_identifier;
    }

    /**
     * @param bool|string $birth_date
     */
    public function setBirthDate($birth_date)
    {
        $this->birth_date = $birth_date;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $email_confirmation
     */
    public function setEmailConfirmation($email_confirmation)
    {
        $this->email_confirmation = $email_confirmation;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @param mixed $id_card_number
     */
    public function setIdCardNumber($id_card_number)
    {
        $this->id_card_number = $id_card_number;
    }

    /**
     * @param mixed $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @param mixed $loan_period
     */
    public function setLoanPeriod($loan_period)
    {
        $this->loan_period = $loan_period;
    }

    /**
     * @param mixed $loan_sum
     */
    public function setLoanSum($loan_sum)
    {
        $this->loan_sum = $loan_sum;
    }

    /**
     * @param mixed $neto_income
     */
    public function setNetoIncome($neto_income)
    {
        $this->neto_income = $neto_income;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param mixed $password_confirmation
     */
    public function setPasswordConfirmation($password_confirmation)
    {
        $this->password_confirmation = $password_confirmation;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param mixed $postal_index
     */
    public function setPostalIndex($postal_index)
    {
        $this->postal_index = $postal_index;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
}