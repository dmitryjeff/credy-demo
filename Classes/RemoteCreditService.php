<?php
/**
 * Created by PhpStorm.
 * User: Dmitri
 * Date: 29.05.14
 * Time: 20:31
 */

namespace Classes;
class RemoteCreditService
{
    /**
     * Turns data into valid JSON format and sends it on external API server.
     */
    public function sendData($customerCredit)
    {
        $data = array(
            'customer' => array(
                'first_name' => $customerCredit->getFirstName(),
                'last_name' => $customerCredit->getLastName(),
                'email' => $customerCredit->getEmail(),
                'email_confirmation' => $customerCredit->getEmailConfirmation(),
                'phone' => $customerCredit->getPhone(),
                'birth_date' => $customerCredit->getBirthDate(),
                'password' => $customerCredit->getPassword(),
                'password_confirmation' => $customerCredit->getPasswordConfirmation(),
                'city' => $customerCredit->getCity(),
                'address' => $customerCredit->getCity(),
                'postal_index' => $customerCredit->getPostalIndex(),
                'neto_income' => $customerCredit->getNetoIncome(),
                'id_card_number' => $customerCredit->getIdCardNumber()
            ),
            'loan' => array(
                'loan_sum' => $customerCredit->getLoanSum(),
                'loan_period' => $customerCredit->getLoanPeriod(),
            ),
            'conditions' => array(
                'accept_general_conditions' => $customerCredit->getAcceptGeneralConditions(),
                'accept_loan_contract' => $customerCredit->getAcceptLoanContract(),
                'accept_terms_personal_data' => $customerCredit->getAcceptTermsPersonalData()
            ),
            'affiliate_identifier' => $customerCredit->getAffiliateIdentifier()
        );
        $content = urlencode(json_encode($data));
        $url = $customerCredit->getUrl();
        $ch = curl_init($url);
# Setup request to send json via POST.
        curl_setopt($ch, CURLOPT_POSTFIELDS, array($content));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
# Return response instead of printing.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
# Send request.
        $result = curl_exec($ch);
        curl_close($ch);
# Print response.

        $response_data = json_decode($result);
        if (isset($response_data['error_message'])) {
            $error_message = $response_data['error_message'];
        } else $error_message = null;
        if (isset($response_data['status'])) {
            $status = $response_data['status'];
        } else $status = null;
        if (isset($response_data['message'])) {
            $message = $response_data['message'];
        } else $message = null;
        if(isset($response_data['customer_id'])){
            $customer_id = $response_data['customer_id'];
        }else $customer_id = null;
        if(isset($response_data['sms_code'])){
            $sms_code = $response_data['sms_code'];
        }else $sms_code = null;

        $customerCreditResponse = new \Classes\CustomerCreditResponse();
        $customerCreditResponse->setErrorMessage($error_message);
        $customerCreditResponse->setStatus($status);
        $customerCreditResponse->setMessage($message);
        $customerCreditResponse->setCustomerId($customer_id);
        $customerCreditResponse->setSmsCode($sms_code);
        return $customerCreditResponse;
    }
}
