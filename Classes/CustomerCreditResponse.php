<?php
/**
 * Created by PhpStorm.
 * User: Dmitri
 * Date: 2.06.14
 * Time: 0:59
 */

namespace Classes;
class CustomerCreditResponse
{
    private $error_message;
    private $status;
    private $message;
    private $customer_id;
    private $sms_code;

    /**
     * Checks response status message and returns error or success message.
     */
    public function getResponseMessage()
    {
        $status = $this->getStatus();
        if ($status!="OK") {
            $status = $this->getStatus();
            $msg = "Error while sending data, status: $status";
            return $msg;
        } else {
            $message = $this->getMessage();
            $customer_id = $this->getCustomerId();
            $sms_code = $this->getSmsCode();
            $msg = "Successful. $message, customer id: $customer_id, sms code: $sms_code";
            return $msg;
        }
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->error_message;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getSmsCode()
    {
        return $this->sms_code;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * @param mixed $customer_id
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
    }

    /**
     * @param mixed $error_message
     */
    public function setErrorMessage($error_message)
    {
        $this->error_message = $error_message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @param mixed $sms_code
     */
    public function setSmsCode($sms_code)
    {
        $this->sms_code = $sms_code;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}