<?php
/**
 * Created by PhpStorm.
 * User: Dmitri
 * Date: 29.05.14
 * Time: 19:40
 */

namespace Classes;
class DataManager
{
    private $_validationErrors;

    public function __construct()
    {
        $this->_validationErrors = array();
    }

    /**
     * Validates all input data. In case of any errors in validation, turns flag on.
     */
    public function dataValidator($customerCredit)
    {
        $email = $customerCredit->getEmail();
        $this->checkEmail($email, 'email');

        $email_confirmation = $customerCredit->getEmailConfirmation();
        $this->checkEmail($email_confirmation, 'email_confirmation');

        $pass = $customerCredit->getPassword();
        $this->checkPass($pass, 'pass');

        $pass = $customerCredit->getPasswordConfirmation();
        $this->checkPass($pass, 'pass_confirmation');

        $fname = $customerCredit->getFirstname();
        $this->checkName($fname, 'fname');

        $lname = $customerCredit->getLastname();
        $this->checkName($lname, 'lname');

        $phone = $customerCredit->getPhone();
        $this->checkNumber($phone, 'phone');

        $city = $customerCredit->getCity();
        $this->checkCity($city);

        $address = $customerCredit->getAddress();
        $this->checkAddress($address);

        $idC = $customerCredit->getIdCardNumber();
        $this->checkNumber($idC, 'idC');

        $loanS = $customerCredit->getLoanSum();
        $this->checkNumber($loanS, 'loanS');

        $loanP = $customerCredit->getLoanPeriod();
        $this->checkNumber($loanP, 'loanPeriod');

        $birth_date = $customerCredit->getBirthDate();
        $this->checkNumber($birth_date, 'birth_date');

        $postal_index = $customerCredit->getPostalIndex();
        $this->checkNumber($postal_index, 'postal_index');

        $neto_income = $customerCredit->getNetoIncome();
        $this->checkNumber($neto_income, 'neto_income');

        $accept_general_conditions = $customerCredit->getAcceptGeneralConditions();
        $this->checkNull($accept_general_conditions);

        $accept_loan_contract = $customerCredit->getAcceptLoanContract();
        $this->checkNull($accept_loan_contract);

        $accept_terms_personal_data = $customerCredit->getAcceptTermsPersonalData();
        $this->checkNull($accept_terms_personal_data);
    }

    /**
     * Validates checkboxes.
     */
    private function checkNull($candidate)
    {
        if (!isset($this->_validationErrors['null'])) {
            $this->_validationErrors['null'] = null;
        }
        if ($candidate) {
            if ($this->_validationErrors['null'] != true)
                $this->_validationErrors['null'] = null;
            else $this->_validationErrors['null'] = true;
        } else  $this->_validationErrors['null'] = true;
    }

    /**
     * Validates E-mail.
     */
    private function checkEmail($email, $fieldname)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->_validationErrors[$fieldname] = true;
        } else {
            $this->_validationErrors[$fieldname] = null;
        }
    }

    /**
     * Validates Password.
     */
    private function checkPass($pass, $fieldname)
    {
        $nameregex = '/^[a-zA-Z0-9]+\s?[a-zA-Z0-9]+$/';
        if (!preg_match($nameregex, $pass))
            $this->_validationErrors[$fieldname] = true;
        else
            $this->_validationErrors[$fieldname] = null;
    }

    /**
     * Validates name.
     */

    private function checkName($name, $fieldName)
    {
        $nameregex = '/^[a-zA-ZÖÄÜÕöäüõ]+\s?[a-zA-ZÖÄÜÕöäüõ]+$/';
        if (!preg_match($nameregex, $name)) {
            $this->_validationErrors[$fieldName] = true;
        } else {
            $this->_validationErrors[$fieldName] = null;
        }
    }

    /**
     * Validates city.
     */
    private function checkCity($city)
    {
        $nameregex = '/^[a-zA-ZÕÜÖÄõüöä]+\s?[a-zA-ZÕÜÖÄõüöä]+$/';
        if (!preg_match($nameregex, $city)) {
            $this->_validationErrors['city'] = true;
        } else $this->_validationErrors['city'] = null;
    }

    /**
     * Validates address.
     */
    private function checkAddress($address)
    {
        $nameregex = '/^[a-zA-Z0-9ÕÜÖÄõüöä -]+\s?[a-zA-Z0-9ÕÜÖÄõüöä -]+$/';
        if (!preg_match($nameregex, $address)) {
            $this->_validationErrors['address'] = true;
        } else $this->_validationErrors['address'] = null;
    }

    /**
     * Validates ID card number, loan sum, loan period, net income, date of birth, postal index, phone number.
     */
    private function checkNumber($field, $fieldname)
    {
        if (is_int($field)) {
            $this->_validationErrors[$fieldname] = true;
        } else  $this->_validationErrors[$fieldname] = null;
    }

    /**
     * @return array
     */
    public function getValidationErrors()
    {
        return $this->_validationErrors;
    }
} 